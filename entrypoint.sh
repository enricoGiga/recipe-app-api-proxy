#!/bin/sh

set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
# by default NGINX run as a background daemon service, that means that when you stop
# it you need to stop the background task too. This is not good with Docker because,
# docker container should run with the primary application
nginx -g 'daemon off;'
