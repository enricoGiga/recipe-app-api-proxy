FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="enrico.gigante@gmail.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static # permission read, write and execute

# create and empty file, it will be
# populated by envsobst when we run the container. It will take our
# template file (default.conf.tpl), will replace the env variables that are defined
# above, and then output it to the location that we have defined.
RUN touch /etc/nginx/conf.d/default.conf

# Lastly, we set ngxix the permissions to modify the file (default.conf)
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint.sh"]
