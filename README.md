# recipe-app-api-proxy
NGINX proxy app for our recipe app API

## Usage


### Environment variables
* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default `app`)
* `APP_PORT` - Port of the app to forward requests to (default_ `9000`)


#default.conf.tpl
This is the NGINX configuration file. This is going to populate variables based on 
the environment variables
